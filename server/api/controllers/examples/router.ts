/* eslint-disable prettier/prettier */
import express from 'express';
import controller from './controller';
export default express
  .Router()
  .post('/:dir', controller.post.bind(controller));
