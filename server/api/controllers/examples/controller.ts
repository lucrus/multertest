/* eslint-disable prettier/prettier */
import { Request, Response } from 'express';
import path from 'path';
import multer from 'multer';

export class Controller {

  filename(_req: Request, file: Express.Multer.File, cb: (error: Error|null, name: string) => void) {
    console.log("file will be named " + file.originalname);
    cb(null, file.originalname);
  }

  post(req: Request, res: Response): void {
  
    console.log("Received POST request");
    const storage = multer.diskStorage({
      destination: path.join('/tmp', req.params.dir), // Trusting req.params.dir is insecure, but this is only an example
      filename: this.filename
    });
    
    const upload = multer(
      { storage: storage, 
        limits: { fieldNameSize: 1000000, 
                  fieldSize: 1000000
                }
      }).single('doc');
    
    console.log("Multer is set to receive a single 'doc' upload");

    upload(req, res, (err: any) => {
        if (err) {
          console.log("Upload failed with error: " + err.message);
          res.status(422);
          res.json(err.message);
          res.end();
        }
      });
  }
}

export default new Controller();
