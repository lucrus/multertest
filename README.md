# multertest

Reproducible example of the 'Unexpected end of form' error. This is a scaffold from express-no-stress-typescript generator with a few lines of code added that should enable the file uploads via Multer.

## Quick Start

```shell
# install deps
npm install

# run in development mode
npm run dev

```

---

## Try It
* Open your browser to [http://localhost:3000](http://localhost:3000)
